% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
  ngerman,
  openany]{book}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  pdftitle={Faktorenanalyse},
  pdfauthor={übersetzt und editiert von Sven Heimbuch},
  pdflang={de},
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\usepackage{longtable,booktabs}
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{5}
\usepackage{booktabs}
\ifxetex
  % Load polyglossia as late as possible: uses bidi with RTL langages (e.g. Hebrew, Arabic)
  \usepackage{polyglossia}
  \setmainlanguage[]{german}
\else
  \usepackage[shorthands=off,main=ngerman]{babel}
\fi
\ifluatex
  \usepackage{selnolig}  % disable illegal ligatures
\fi
\usepackage[]{natbib}
\bibliographystyle{apalike}

\title{Faktorenanalyse}
\usepackage{etoolbox}
\makeatletter
\providecommand{\subtitle}[1]{% add subtitle to \maketitle
  \apptocmd{\@title}{\par {\large #1 \par}}{}{}
}
\makeatother
\subtitle{Kapitel 15 aus Learning statistics with jamovi von Navarro D. J., \& Foxcroft D. R. (2019)}
\author{übersetzt und editiert von Sven Heimbuch}
\date{2020-10-07}

\begin{document}
\maketitle

{
\setcounter{tocdepth}{1}
\tableofcontents
}
\hypertarget{einfuxfchrung}{%
\chapter*{Einführung}\label{einfuxfchrung}}
\addcontentsline{toc}{chapter}{Einführung}

Zuvor wurden statistische Tests für Unterschiede zwischen zwei oder mehr Gruppen behandelt. Manchmal möchten wir jedoch bei der Durchführung von Forschungsarbeiten untersuchen, wie mehrere Variablen kovariieren. Das heißt, wie sie zueinander in Beziehung stehen und ob die Muster der Verwandtschaft etwas Interessantes und Bedeutungsvolles nahelegen. Zum Beispiel sind wir oft daran interessiert zu untersuchen, ob es zugrunde liegende, nicht beobachtete latente Faktoren gibt, die durch die beobachteten, direkt gemessenen Variablen in unserem Datensatz repräsentiert werden. In der Statistik sind latente Faktoren zunächst verborgene Variablen, die nicht direkt beobachtet werden, sondern (durch statistische Analyse) aus anderen beobachteten (direkt gemessenen) Variablen abgeleitet werden.

In diesem Kapitel werden wir eine Reihe verschiedener Faktorenanalysen und verwandter Techniken betrachten, beginnend mit der Explorativen Faktorenanalyse (EFA). Die EFA ist eine statistische Technik zur Identifizierung zugrunde liegender latenter Faktoren in einem Datensatz (Abschnitt 1). In Abschnitt 2 werden wir uns dann mit der Hauptkomponentenanalyse (PCA = Principal Component Analysis) befassen, bei der es sich um eine Datenreduktionstechnik handelt, die streng genommen keine zugrundeliegenden latenten Faktoren identifiziert. Stattdessen erzeugt die PCA einfach eine Linearkombination von beobachteten Variablen. Im Anschluss daran zeigt Abschnitt 3 über die Konfirmatorische Faktorenanalye (CFA = Confirmatory Factor Analysis), dass Sie bei der CFA im Gegensatz zur EFA mit einer Idee - einem Modell - beginnen, wie die Variablen in Ihren Daten miteinander in Beziehung stehen. Dann testen Sie Ihr Modell anhand der beobachteten Daten und beurteilen, wie gut das Modell passt. Eine verfeinerte Version der CFA ist der so genannte Multi-Trait-Multi-Method (MTMM)-Ansatz (Abschnitt 4), bei dem sowohl latente Faktoren- als auch Methodenvarianz in das Modell einbezogen werden. Dies ist nützlich, wenn verschiedene methodische Ansätze für die Messung verwendet werden und daher die Methodenvarianz ein wichtiger Gesichtspunkt ist. Schließlich werden wir eine verwandte Analyse behandeln: Die Reliabilitätsanalyse prüft, wie konsistent eine Skala ein psychologisches Konstrukt misst (Abschnitt 5).

\hypertarget{explorative-faktorenanalyse}{%
\chapter{Explorative Faktorenanalyse}\label{explorative-faktorenanalyse}}

Die explorative Faktorenanalyse (EFA) ist ein statistisches Verfahren zur Aufdeckung aller versteckten latenten Faktoren, die aus unseren Beobachtungsdaten abgeleitet werden können. Mit dieser Technik wird berechnet, inwieweit eine bestimmte Gruppe von gemessenen Variablen, z.B. \emph{V\textsubscript{1}}, \emph{V\textsubscript{2}}, \emph{V\textsubscript{3}}, \emph{V\textsubscript{4}} und \emph{V\textsubscript{5}}, als Maße eines zugrunde liegenden latenten Faktors dargestellt werden können. Dieser latente Faktor kann nicht durch nur eine beobachtete Variable gemessen werden, sondern manifestiert sich stattdessen in den Beziehungen, die er in einer Reihe beobachteter Variablen verursacht.

In Abbildung \ref{fig:fa1} wird jede beobachtete Variable \emph{V} bis zu einem gewissen Grad durch den zugrundeliegenden latenten Faktor (\emph{F}) ``verursacht'', dargestellt durch die Koeffizienten \emph{b\textsubscript{1}} bis \emph{b\textsubscript{5}} (auch Faktorladungen genannt). Jede beobachtete Variable hat auch einen zugehörigen Fehlerterm, \emph{e\textsubscript{1}} bis \emph{e\textsubscript{5}}. Jeder Fehlerterm ist die Varianz innerhalb der zugehörigen beobachteten Variablen \emph{V\textsubscript{i}}, die durch den zugrundeliegenden latenten Faktor nicht erklärt wird.

\begin{figure}

{\centering \includegraphics[width=0.74\linewidth]{img/fa1} 

}

\caption{Latenter Faktor \emph{F}, der einer Beziehung zwischen mehreren beobachteten Variablen \emph{V\textsubscript{i}} zugrunde liegt.}\label{fig:fa1}
\end{figure}



In der Psychologie stellen latente Faktoren psychologische Phänomene oder Konstrukte dar, die schwer direkt zu beobachten oder zu messen sind. Zum Beispiel die Persönlichkeit, die Intelligenz oder der Denkstil. Im Beispiel in Abbildung \ref{fig:fa1} haben wir Menschen vielleicht fünf spezifische Fragen über ihr Verhalten oder ihre Einstellungen gestellt, und daraus können wir uns ein Bild über ein Persönlichkeitskonstrukt machen, das z.B. Extraversion genannt wird. Ein anderer Satz spezifischer Fragen kann uns ein Bild über die Introvertiertheit einer Person oder ihre Gewissenhaftigkeit vermitteln.

Hier ist ein weiteres Beispiel: Wir sind vielleicht nicht in der Lage, die Statistikangst direkt zu messen, aber wir können mit einer Reihe von Fragen in einem Fragebogen messen, ob die Statistikangst hoch oder niedrig ist. Zum Beispiel ``Q1: Durchführung der Aufgabe für einen Statistikkurs'', ``Q2: Versuch, die in einem Zeitschriftenartikel beschriebene Statistik zu verstehen'' und ``Q3: Den Dozenten um Hilfe bitten, um etwas aus dem Kurs zu verstehen'' usw., jeweils mit einer Bewertung von niedriger Angst bis hoher Angst. Personen mit hoher Statistikangst werden aufgrund ihrer hohen Statistikangst dazu neigen, ähnlich hohe Antworten auf diese beobachteten Variablen zu geben. Ebenso werden Menschen mit niedriger Statistikangst aufgrund ihrer niedrigen Statistikangst ähnlich niedrige Antworten auf diese Variablen geben.

Bei der explorativen Faktorenanalyse (EFA) untersuchen wir im Wesentlichen die Korrelationen zwischen beobachteten Variablen, um alle interessanten, wichtigen zugrunde liegenden (latenten) Faktoren aufzudecken, die identifiziert werden, wenn beobachtete Variablen miteinander kovariieren. Wir können statistische Software verwenden, um alle latenten Faktoren abzuschätzen und zu identifizieren, welche unserer Variablen eine hohe Ladung\footnote{Es ist für uns sehr hilfreich, dass Faktorladungen wie standardisierte Regressionskoeffizienten interpretiert werden können.} haben (z.B. Faktorladung \textgreater{} 0.5) auf jeden Faktor interpretiert werden können, was darauf hindeutet, dass sie ein nützliches Maß oder einen nützlichen Indikator für den latenten Faktor darstellen. Ein Teil dieses Prozesses umfasst einen Schritt, der Rotation genannt wird, was ehrlich gesagt eine ziemlich seltsame Idee ist, aber glücklicherweise müssen wir uns nicht darum kümmern, sie vollständig in allen Details zu verstehen; wir müssen nur wissen, dass sie hilfreich ist, weil sie das Muster der Ladungen auf verschiedene Faktoren viel klarer macht. So hilft die Rotation dabei, klarer zu sehen, welche Variablen inhaltlich mit jedem Faktor verbunden sind. Wir müssen auch entscheiden, wie viele Faktoren angesichts unserer Daten vernünftig sind, und hilfreich in dieser Hinsicht ist etwas, das Eigenwerte genannt wird. Wir werden gleich darauf zurückkommen, nachdem wir einige der Hauptvoraussetzungen der EFA behandelt haben.

\hypertarget{voraussetzungen-der-efa}{%
\section{Voraussetzungen der EFA}\label{voraussetzungen-der-efa}}

Es gibt eine Reihe von Annahmen, die im Rahmen der Analyse überprüft werden müssen. Die erste Annahme ist die Sphärizität, die im Wesentlichen prüft, ob die Variablen in Ihrem Datensatz so weit miteinander korreliert sind, dass sie potenziell mit einem kleineren Satz von Faktoren zusammengefasst werden können. Der Bartlett-Test auf Sphärizität prüft, ob die beobachtete Korrelationsmatrix signifikant von einer Nullkorrelationsmatrix abweicht. Wenn also der Bartlett-Test signifikant ist (\emph{p} \textless{} .05), deutet dies darauf hin, dass die beobachtete Korrelationsmatrix signifikant von der Nullkorrelationsmatrix abweicht und daher für die EFA geeignet ist.

Die zweite Annahme ist die Stichprobenadäquanz (MSA = Measure of Sampling Adequacy) und wird mit dem Kaiser-Meyer-Olkin (KMO)-Maß überprüft. Der KMO-Index ist ein Maß für den Anteil der Varianz unter den beobachteten Variablen, der eine gemeinsame Varianz sein könnte. Mit Hilfe partieller Korrelationen prüft er auf Faktoren, die nur von zwei Items geladen werden. Selten, wenn überhaupt, wollen wir, dass die EFA viele Faktoren produziert, die jeweils von nur zwei Items geladen werden. Bei der KMO geht es um die Stichprobenadäquanz, da partielle Korrelationen typischerweise mit inadäquaten Stichproben gesehen werden. Wenn der KMO-Index hoch ist (≈ 1), ist die EFA effizient, während bei einem niedrigen KMO-Index (≈ 0) die EFA nicht relevant ist. KMO-Werte kleiner als 0.5 zeigen an, dass die EFA nicht geeignet ist, und ein KMO-Wert von etwa 0.6 sollte vorhanden sein, bevor die EFA als geeignet betrachtet wird. Werte zwischen 0.5 und 0.7 gelten als angemessen, Werte zwischen 0.7 und 0.9 als gut und Werte zwischen 0.9 und 1.0 als ausgezeichnet.

\hypertarget{wozu-ist-die-efa-gut}{%
\section{Wozu ist die EFA gut?}\label{wozu-ist-die-efa-gut}}

Wenn die EFA eine gute Lösung (d.h. ein Faktorenmodell) geliefert hat, dann müssen wir entscheiden, was wir mit unseren schönen neuen Faktoren machen. Forscher verwenden die EFA oft für die Erstellung psychometrischer Skalen. Sie entwickeln einen Pool von Fragebogenitems, die sich ihrer Meinung nach auf ein oder mehrere psychologische Konstrukte beziehen, verwenden die EFA, um zu sehen, welche Items als latente Faktoren ``zusammenpassen'', und dann beurteilen sie, ob einige Items entfernt werden sollten, weil sie einen der latenten Faktoren nicht sinnvoll oder nicht eindeutig messen.

In Übereinstimmung mit diesem Ansatz besteht eine weitere Konsequenz der EFA darin, die Variablen, die verschiedene Faktoren belasten, zu einem Faktor-Score, manchmal auch als Skalen-Score bezeichnet, zu kombinieren. Es gibt zwei Möglichkeiten, Variablen zu einem Skalen-Score zu kombinieren:

\begin{itemize}
\tightlist
\item
  Erstellen Sie eine neue Variable mit einer Punktzahl, die mit den Faktorladungen für jedes Element gewichtet ist, das zu dem Faktor beiträgt.
\item
  Erstellen Sie eine neue Variable auf der Grundlage jedes Items, das zu dem Faktor beiträgt, wobei diese jedoch gleich gewichtet werden.
\end{itemize}

Bei der ersten Option hängt der Beitrag jedes Items zur Gesamtpunktzahl davon ab, wie stark es sich auf den Faktor bezieht. Bei der zweiten Option wird in der Regel nur der Durchschnitt über alle Items gebildet, die wesentlich zu einem Faktor beitragen, um die kombinierte Skalen-Score-Variable zu bilden. Für welche Option man sich entscheidet, ist eine Frage der Präferenz, wobei ein Nachteil der ersten Option darin besteht, dass die Ladungen von Stichprobe zu Stichprobe ziemlich stark variieren können, und in den Verhaltens- und Lebenswissenschaften sind wir oft daran interessiert, kombinierte Fragebogen-Score-Skalenwerte für verschiedene Studien und verschiedene Stichproben zu entwickeln und zu verwenden. In diesem Fall ist es vernünftig, ein zusammengesetztes Maß zu verwenden, das auf den substanziellen Items basiert, die gleich viel beitragen, anstatt eine Gewichtung durch stichprobenspezifische Ladungen aus einer anderen Stichprobe vorzunehmen. In jedem Fall ist es einfacher und intuitiver, ein kombiniertes variables Maß als Mittelwert der Items zu verstehen, als eine stichprobenspezifische optimal gewichtete Kombination zu verwenden.

Eine fortgeschrittenere statistische Technik, die den Rahmen dieser Einführung aber eindeutig sprengen würde, ist die Regressionsmodellierung, bei der latente Faktoren in Vorhersagemodellen für andere latente Faktoren verwendet werden. Dies wird als ``Strukturgleichungsmodellierung'' (SEM = Structural Equation Modelling) bezeichnet, und es gibt spezielle Softwareprogramme und R-Pakete, die sich diesem Ansatz widmen. Aber wir sollten nichts überstürzen; worauf wir uns jetzt wirklich konzentrieren sollten, ist die Frage, wie man eine EFA in jamovi durchführt.

\hypertarget{efa-in-jamovi}{%
\section{EFA in jamovi}\label{efa-in-jamovi}}

Zunächst benötigen wir einige Daten. Fünfundzwanzig Persönlichkeits-Selbstberichts-Items (siehe Tabelle \ref{tab:bfi}) aus dem International Personality Item Pool (\url{https://ipip.ori.org}) wurden im Rahmen des webbasierten Persönlichkeitsbewertungsprojekts Synthetic Aperture Personality Assessment (SAPA) (SAPA: \url{https://sapa-project.org}) aufgenommen. Die 25 Items sind nach fünf vermuteten Faktoren geordnet: Verträglichkeit, Gewissenhaftigkeit, Extraversion, Neurotizismus und Offenheit. Die Itemdaten wurden anhand einer 6-Punkte-Antwortskala (1 = trifft überhaupt nicht zu -- 6 = trifft vollkommen zu) erhoben.

\begin{longtable}[]{@{}lll@{}}
\caption{\label{tab:bfi} Beobachtete Variablen-Items, geordnet nach den fünf vermuteten Persönlichkeitsfaktoren im Datensatz.}\tabularnewline
\toprule
Variable & Item & Kodierung\tabularnewline
\midrule
\endfirsthead
\toprule
Variable & Item & Kodierung\tabularnewline
\midrule
\endhead
A1 & Am indifferent to the feelings of others. & R\tabularnewline
A2 & Inquire about other's well-being. &\tabularnewline
A3 & Know how to comfort others. &\tabularnewline
A4 & Love children. &\tabularnewline
A5 & Make people feel at ease. &\tabularnewline
C1 & Am exacting at my work. &\tabularnewline
C2 & Continue until everything is perfect. &\tabularnewline
C3 & Do things according to a plan. &\tabularnewline
C4 & Do things in a half-way manner. & R\tabularnewline
C5 & Waste my time. & R\tabularnewline
E1 & Don't talk a lot. & R\tabularnewline
E2 & Find it difficult to approach others. & R\tabularnewline
E3 & Know how to captivate people. &\tabularnewline
E4 & Make friends easily. &\tabularnewline
E5 & Take charge. &\tabularnewline
N1 & Get angry easily. &\tabularnewline
N2 & Get irritated easily. &\tabularnewline
N3 & Have frequent mood swings. &\tabularnewline
N4 & Often feel blue. &\tabularnewline
N5 & Panic easily. &\tabularnewline
O1 & Am full of ideas. &\tabularnewline
O2 & Avoid difficult reading material. & R\tabularnewline
O3 & Carry the conversation to a higher level. &\tabularnewline
O4 & Spend time reflecting on things. &\tabularnewline
O5 & Will not probe deeply into a subject. & R\tabularnewline
\bottomrule
\end{longtable}

Die Antworten aus eine Stichprobe von \emph{N} = 250 Personen ist im Datensatz bfi\_sample.omv enthalten. Als Forscher sind wir daran interessiert, die Daten zu untersuchen, um zu sehen, ob es einige zugrunde liegende latente Faktoren gibt, die von den 25 beobachteten Variablen in der Datei bfi\_sample.omv einigermaßen gut gemessen werden. Öffnen Sie den Datensatz und überprüfen Sie, ob die 25 Variablen als kontinuierliche Variablen kodiert sind (technisch gesehen sind sie ordinal, obwohl es für die EFA in jamovi meistens keine Rolle spielt, außer wenn Sie sich entscheiden, gewichtete Faktor-Scores zu berechnen, in welchem Fall kontinuierliche Variablen erforderlich sind). So führen Sie die EFA in jamovi durch:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Öffnen Sie den Datensatz \href{data/bfi_sample.omv}{bfi\_sample.omv}
\item
  Wählen Sie \textbf{Factor} -- \textbf{Exploratory Factor Analysis} aus der jamovi-Schaltflächenleiste, um das EFA-Analysefenster zu öffnen (Abbildung \ref{fig:fa2}).
\item
  Wählen Sie die 25 Persönlichkeitsfragen aus und übertragen Sie sie in das Feld ``Variables''.
\item
  Prüfen Sie geeignete Optionen, einschließlich \emph{Assumption Checks}, aber auch die Optionen \emph{Method} unter dem Punkt \emph{Rotation}, \emph{Number of Factors} zum Extrahieren und \emph{Additional Output}. Siehe Abbildung XY für vorgeschlagene Optionen für diese beispielhafte EFA, und beachten Sie bitte, dass die Rotationsmethode und Anzahl der Faktoren, die extrahiert werden, normalerweise vom Forscher während der Analyse angepasst werden, um das beste Ergebnis zu finden.
\end{enumerate}

\begin{figure}

{\centering \includegraphics[width=0.74\linewidth]{img/fa2} 

}

\caption{Die Einstellungen für eine Explorative Faktorenanalyse in jamovi.}\label{fig:fa2}
\end{figure}

Überprüfen Sie zunächst die Voraussetzungen (Abbildung \ref{fig:fa3}). Sie können sehen, dass (1) der Bartlett-Test auf Sphärizität signifikant ist (\emph{p} \textless{} .05), so dass diese Annahme erfüllt ist; und (2) das KMO-Maß der Stichprobenadäquanz (MSA) insgesamt 0.81 beträgt, was auf eine gute Stichprobenadäquanz hindeutet. Hier gibt es also keine Probleme! Als nächstes ist zu prüfen, wie viele Faktoren zu verwenden (oder aus den Daten zu ``extrahieren'') sind. Es stehen drei verschiedene Ansätze zur Verfügung:

\begin{figure}

{\centering \includegraphics[width=0.42\linewidth]{img/fa3} 

}

\caption{Prüfung der Voraussetzungen einer EFA für die Daten des Persönlichkeitsfragebogens.}\label{fig:fa3}
\end{figure}

\begin{itemize}
\tightlist
\item
  Eine Konvention besteht darin, alle Komponenten mit Eigenwerten größer als 1 zu wählen\footnote{Ein Eigenwert gibt an, wie viel der Varianz in den beobachteten Variablen ein Faktor ausmacht. Ein Faktor mit einem Eigenwert \textgreater{} 1 erklärt mehr Varianz als eine einzelne beobachtete Variable}. Damit würden wir mit unseren Daten vier Faktoren erhalten (probieren Sie es aus und sehen Sie). Dieses Kriterium ist in der Fachliteratur auch als Kaiser-Kriterium bekannt.
\item
  Untersuchung des Scree-Plots, wie in Abbildung \ref{fig:fa4}, können Sie den ``Wendepunkt'' identifizieren. Dies ist der Punkt, an dem sich die Neigung des Linienverlaufs unterhalb des ``Wendepunktes'' deutlich abflacht. Dies würde uns mit unseren Daten fünf Faktoren liefern. Die Interpretation von Scree-Plots ist eine Kunst: In Abbildung \ref{fig:fa4} gibt es einen merklichen Stufensprung von 5 auf 6 Faktoren, aber in anderen Scree-Plots, die Sie sich anschauen, wird es nicht so eindeutig sein.
\item
  Unter Verwendung der sogenannten \emph{Parallel Analysis} werden die erhaltenen Eigenwerte mit denen verglichen, die sich aus simulierten Zufallsdaten ergeben würden. Die Anzahl der extrahierten Faktoren ist die Anzahl mit Eigenwerten, die größer ist als die Anzahl, die mit Zufallsdaten gefunden würde.
\end{itemize}

\begin{figure}

{\centering \includegraphics[width=0.84\linewidth]{img/fa4} 

}

\caption{Scree-Plot der Persönlichkeitsdaten, der eine auffällige Beuge und ein Abflachen nach Punkt 5 (dem "Ellbogen") zeigt.}\label{fig:fa4}
\end{figure}

Der dritte Ansatz ist nach Fabrigar, Wegener, MacCallum und Strahan (1999) ein guter Ansatz, obwohl in der Praxis die Forscher dazu neigen, alle drei zu betrachten und dann ein Urteil über die Anzahl der Faktoren zu fällen, die am leichtesten oder hilfreichsten zu interpretieren sind. Dies kann als das" Sinnhaftigkeitskriterium" verstanden werden, und die Forscher werden in der Regel zusätzlich zu der Lösung aus einem der oben genannten Ansätze Lösungen mit einem oder zwei zusätzlichen oder weniger Faktoren untersuchen. Sie übernehmen dann die Lösung, die für sie am sinnvollsten ist.

Gleichzeitig sollten wir auch überlegen, wie wir die finale Lösung am besten rotieren lassen können. Es gibt zwei Hauptansätze für die Rotation: eine orthogonale (z.B. `varimax') Rotation erzwingt, dass die ausgewählten Faktoren unkorreliert sind, während eine schiefe (z.B. `oblimin') Rotation erlaubt, dass die gewählten Faktoren miteinander korrelieren. Dimensionen, die für Psychologen und Verhaltensforscher von Interesse sind, sind selten Dimensionen, von denen wir erwarten würden, dass sie orthogonal sind, so dass schiefe Lösungen\footnote{Schiefe Rotationen liefern zwei Faktormatrizen, eine als Strukturmatrix und eine als Mustermatrix bezeichnet. In jamovi wird in den Ergebnissen nur die Mustermatrix gezeigt, da diese typischerweise am nützlichsten für die Interpretation ist, obwohl einige Experten meinen, dass beide hilfreich sein können. In einer Strukturmatrix zeigen die Koeffizienten die Beziehung zwischen der Variablen und den Faktoren an, während sie die Beziehung dieses Faktors zu allen anderen Faktoren ignorieren (d.h. eine Korrelation nullter Ordnung). In einer Strukturmatrix zeigen Koeffizienten den einzigartigen Beitrag eines Faktors zu einer Variablen, während sie die Auswirkungen anderer Faktoren auf diese Variable kontrollieren (ähnlich wie ein standardisierter partieller Regressionskoeffizient). Unter orthogonaler Rotation sind Struktur- und Musterkoeffizienten gleich.} vermutlich sinnvoller sind.

Wenn sich in der Praxis herausstellt, dass bei einer schiefen Rotation die Faktoren miteinander deutlich korrelieren (positiv oder negativ, und \textgreater{} 0,3), wie in Abbildung \ref{fig:fa5}, wo eine Korrelation zwischen zwei der extrahierten Faktoren 0.398 beträgt, dann würde dies unsere Intuition bestätigen, die schiefe Rotation zu bevorzugen. Wenn die Faktoren tatsächlich korreliert sind, dann wird eine schiefe Rotation eine bessere Schätzung der wahren Faktoren und eine bessere vereinfachte Struktur ergeben als eine orthogonale Rotation. Und wenn die schiefe Rotation darauf hindeutet, dass die Faktoren nahezu null Korrelationen untereinander haben, dann kann der Forscher eine orthogonale Rotation durchführen (die dann ungefähr die gleiche Lösung wie die schiefe Rotation ergeben sollte).

\begin{figure}

{\centering \includegraphics[width=0.74\linewidth]{img/fa5} 

}

\caption{Zusammenfassende Faktorstatistiken und Korrelationen für eine Fünf-Faktoren-Lösung.}\label{fig:fa5}
\end{figure}

Bei der Überprüfung der Korrelation zwischen den extrahierten Faktoren war mindestens eine Korrelation größer als 0.3 (Abbildung \ref{fig:fa5}), so dass eine schiefe (``oblimin'') Rotation der fünf extrahierten Faktoren bevorzugt wird.

In Abbildung \ref{fig:fa5} sehen wir auch, dass der Anteil der fünf Faktoren an der Gesamtvarianz der Daten 46\% beträgt. Faktor eins macht etwa 10\% der Varianz aus, die Faktoren zwei bis vier jeweils etwa 9\% und Faktor fünf etwas mehr als 7\%. Das ist nicht großartig; es wäre besser gewesen, wenn die Gesamtlösung einen substantielleren Anteil an der Varianz unserer Daten gehabt hätte.

Seien Sie sich bewusst, dass Sie in jeder EFA potenziell die gleiche Anzahl von Faktoren wie beobachtete Variablen haben könnten, aber jeder zusätzliche Faktor, den Sie einbeziehen, wird einen geringeren Anteil der erklärten Varianz hinzufügen. Wenn die ersten paar Faktoren einen guten Teil der Varianz in den ursprünglichen 25 Variablen erklären, dann sind diese Faktoren eindeutig ein nützlicher, einfacherer Ersatz für die 25 Variablen. Sie können den Rest weglassen, ohne zu viel von der ursprünglichen Variabilität zu verlieren. Wenn jedoch 18 Faktoren (zum Beispiel) erforderlich sind, um den größten Teil der Varianz in diesen 25 Variablen zu erklären, können Sie auch einfach die ursprünglichen 25 verwenden.

Abbildung \ref{fig:fa6} zeigt die Faktorladungen. Das heißt, wie die 25 verschiedenen Persönlichkeitsitems auf jeden der fünf ausgewählten Faktoren laden. Wir haben Ladungen versteckt, die geringer als 0.3 sind (was wir in den in Abbildung \ref{fig:fa2} gezeigten Optionen festgelegt haben).

\begin{figure}

{\centering \includegraphics[width=0.84\linewidth]{img/fa6} 

}

\caption{Faktorladungen für eine Fünf-Faktor-Lösung.}\label{fig:fa6}
\end{figure}

Für die Faktoren 1, 2, 3 und 4 stimmt das Muster der Faktorladungen eng mit den in Tabelle \ref{tab:bfi} angegebenen mutmaßlichen Faktoren überein. Puh! Und Faktor 5 liegt ziemlich nahe beieinander, wobei vier der fünf beobachteten Variablen, die vermeintlich die ``Offenheit'' messen, ziemlich gut auf den Faktor geladen werden. Die Variable \emph{O\textsubscript{4}} scheint jedoch nicht ganz zu passen, da die Faktorlösung in Abbildung \ref{fig:fa6} nahelegt, dass sie auf Faktor 3 (wenn auch mit einer relativ geringen Ladung), aber nicht substanziell auf Faktor 5 lädt.

Außerdem ist zu beachten, dass die Variablen, die in Tabelle \ref{tab:bfi} in der Spalte Kodierung mit ``R'' (Reverse Coding) bezeichnet wurden, diejenigen sind, die negative Faktorladungen aufweisen. Werfen Sie einen Blick auf die Items \emph{A\textsubscript{1}} (``Am indifferent to the feelings of others'') und \emph{A\textsubscript{2}} (``Inquire about other's well-being.''). Wir können sehen, dass eine hohe Punktzahl bei A1 eine niedrige Verträglichkeit anzeigt, während eine hohe Punktzahl bei \emph{A\textsubscript{2}} (und allen anderen \emph{A}-Variablen) eine hohe Verträglichkeit anzeigt. Daher wird \emph{A\textsubscript{1}} negativ mit den anderen \emph{A}-Variablen korreliert sein, und aus diesem Grund hat es eine negative Faktorladung, wie in Abbildung \ref{fig:fa6} dargestellt.

In Abbildung \ref{fig:fa6} sehen wir auch die Uniqueness (Einzigartigkeit) jeder Variablen. Uniqueness ist der Anteil der Varianz, der für die Variable ``einzigartig'' ist und nicht durch die Faktoren erklärt wird\footnote{Manchmal wird in der Faktorenanalyse von ``Kommunalität'' berichtet, d.h. von der Menge der Varianz in einer Variable, die durch die Faktorlösung erklärt wird. \emph{Uniqueness = 1 -- Kommunalität}}. Beispielsweise werden 74\% der Varianz in \emph{A\textsubscript{1}} nicht durch die Faktoren in der Fünf-Faktoren-Lösung erklärt. Im Gegensatz dazu hat `N1' eine relativ geringe Varianz, die nicht durch die Faktorlösung erklärt wird (31\%). Beachten Sie, dass die Relevanz oder der Beitrag der Variable im Faktormodell umso geringer ist, je größer die ``Eindeutigkeit'' ist.

Um ehrlich zu sein, ist es ungewöhnlich, eine so saubere Lösung in der EFA zu erhalten. Normalerweise ist sie etwas verworrener als diese, und die Interpretation der Bedeutung der Faktoren ist oft schwieriger. Es kommt nicht oft vor, dass Sie einen so klar abgegrenzten Item-Pool haben. Häufiger haben Sie einen ganzen Haufen beobachteter Variablen, von denen Sie denken, dass sie Indikatoren für einige zugrunde liegende latente Faktoren sein könnten, aber Sie haben nicht so ein ausgeprägtes Gespür dafür, welche Variablen wohin gehen werden!

Wir scheinen also eine ziemlich gute Fünf-Faktoren-Lösung zu haben, auch wenn diese einen relativ geringen Anteil an der beobachteten Varianz insgesamt ausmacht. Nehmen wir an, wir sind mit dieser Lösung zufrieden und wollen unsere Faktoren in der weiteren Analyse verwenden. Die einfachste Möglichkeit besteht darin, einen Gesamt-(Durchschnitts-)Score für jeden Faktor zu berechnen, indem der Score für jede Variable, die den Faktor substanziell belastet, addiert und dann durch die Anzahl der Variablen geteilt wird. Für jede Person in unserem Datensatz würde das bedeuten, dass z.B. für den Faktor Verträglichkeit \emph{A\textsubscript{1}} + \emph{A\textsubscript{2}} + \emph{A\textsubscript{3}} + \emph{A\textsubscript{4}} + \emph{A\textsubscript{5}} addiert und dann durch 5 geteilt wird{[} wobei zu beachten ist, dass einige Variablen, falls erforderlich, zuerst umgekehrt bewertet werden müssen{]}. Im Wesentlichen bedeutet dies, dass der von uns berechnete Faktor-Score auf gleich gewichteten Scores von jeder der einbezogenen Variablen basiert. Wir können dies in jamovi in zwei Schritten tun:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  \emph{A\textsubscript{1}} in eine Variable \emph{A\textsubscript{1R}} umkodieren, indem die Werte mit der Funktion ``Transform'' (siehe Abbildung \ref{fig:fa7}) umgekehrt kodiert werden (d.h. 6 -\textgreater{} 1; 5 -\textgreater{} 2; 4 -\textgreater{} 3; 3 -\textgreater{} 4; 2 -\textgreater{} 5; 1 -\textgreater{} 6).
\end{enumerate}

\begin{figure}

{\centering \includegraphics[width=0.84\linewidth]{img/fa7} 

}

\caption{Variable mit der Funktion "Transform" umkodieren.}\label{fig:fa7}
\end{figure}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Berechnen Sie eine neue Variable, die als ``Agreeableness'' bezeichnet wird, indem Sie den Mittelwert von \emph{A\textsubscript{1R}}, \emph{A\textsubscript{2}}, \emph{A\textsubscript{3}}, \emph{A\textsubscript{4}} und \emph{A\textsubscript{5}} berechnen. Verwenden Sie dazu die Funktion ``Compute'' (siehe Abbildung \ref{fig:fa8}).
\end{enumerate}

\begin{figure}

{\centering \includegraphics[width=0.84\linewidth]{img/fa8} 

}

\caption{Berechnen Sie eine neue Score-Variable mit der Funktion "Compute".}\label{fig:fa8}
\end{figure}

Eine weitere Möglichkeit besteht darin, einen optimal gewichteten Faktor-Score-Index zu erstellen. Wir können den jamovi Rj-Editor verwenden, um dies in R zu tun. Auch hier gibt es zwei Schritte:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Verwenden Sie das Modul Rj-Editor, um die EFA in R mit der gleichen Spezifizierung wie die EFA in jamovi (d.h. fünf Faktoren und keine Rotation) auszuführen und optimal gewichtete Faktor-Scores zu berechnen. Speichern Sie den neuen Datensatz mit den Faktor-Scores in einer Datei. Siehe Abbildung \ref{fig:fa9}.
\end{enumerate}

\begin{figure}

{\centering \includegraphics[width=0.98\linewidth]{img/fa9} 

}

\caption{Befehle des Rj-Editors zum Erstellen optimal gewichteter Faktor-Scores für die Fünf-Faktoren-Lösung.}\label{fig:fa9}
\end{figure}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Öffnen Sie die neue Datei (siehe Abbildung \ref{fig:fa10}) in jamovi und überprüfen Sie, ob die Variablentypen korrekt eingestellt wurden. Beschriften Sie die neuen Faktor-Score-Variablen, die den entsprechenden Faktornamen oder Definitionen entsprechen (es ist möglich, dass die Faktoren nicht in der erwarteten Reihenfolge vorliegen, überprüfen Sie dies also unbedingt).
\end{enumerate}

\begin{figure}

{\centering \includegraphics[width=0.84\linewidth]{img/fa10} 

}

\caption{Die neu erstellte Datendatei "bfifactscores.csv", die im Rj-Editor erstellt wurde und die fünf Faktor-Score-Variablen enthält. Beachten Sie, dass jede der neuen Faktor-Score-Variablen entsprechend der Reihenfolge, in der die Faktoren in der Faktorladungstabelle aufgeführt sind, beschriftet ist.}\label{fig:fa10}
\end{figure}

Nun können Sie weitere Analysen durchführen, entweder mit den Faktor-basierten Scores (ein Mittelwert-Score-Ansatz) oder mit den optimal gewichteten Faktor-Scores, die über den Rj-Editor berechnet wurden. Ihre Wahl! Sie können zum Beispiel prüfen, ob es in jeder unserer Persönlichkeitsskalen geschlechtsspezifische Unterschiede gibt. Wir taten dies für den Punktwert ``Verträglichkeit'', den wir mit dem Faktor-basierten Score-Ansatz berechnet haben, und obwohl die Darstellung (Abbildung \ref{fig:fa11}) zeigte, dass Männer weniger verträglich waren als Frauen, war dies kein signifikanter Unterschied (Mann-Whitney \emph{U} = 5760.5, \emph{p} = .073).

\begin{figure}

{\centering \includegraphics[width=0.65\linewidth]{img/fa11} 

}

\caption{Vergleich der Unterschiede in den auf dem Faktor Verträglichkeit basierenden Punktzahlen zwischen Männern und Frauen}\label{fig:fa11}
\end{figure}

\hypertarget{aufschreiben-einer-efa}{%
\section{Aufschreiben einer EFA}\label{aufschreiben-einer-efa}}

Hoffentlich haben wir Ihnen bisher ein Gefühl für die EFA vermittelt und gezeigt, wie man die EFA in jamovi durchführen kann. Wenn Sie also Ihre EFA abgeschlossen haben, wie schreiben Sie sie auf? Es gibt keinen formellen Standardweg zur Erstellung einer EFA, und die Beispiele sind je nach Disziplin und Forscher unterschiedlich. Dennoch gibt es einige recht standardmäßige Informationen, die Sie in Ihren Bericht aufnehmen sollten:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Welches sind die theoretischen Grundlagen für den Bereich, den Sie untersuchen, und insbesondere für die Konstrukte, die Sie mit Hilfe der EFA aufdecken möchten?
\item
  Eine Beschreibung der Stichprobe (z.B. demographische Informationen, Stichprobengröße, Sampling-Methode).
\item
  Eine Beschreibung der Art der verwendeten Daten (z.B. nominal, kontinuierlich) und der deskriptiven Statistiken.
\item
  Beschreiben Sie, wie Sie die Voraussetzungen für die EFA getestet haben. Einzelheiten über Sphärizitätsprüfungen und Maße der Stichprobenadäquanz sollten berichtet werden.
\item
  Erklären Sie, welche FA-Extraktionsmethode (z.B. Maximum Likelihood) verwendet wurde.
\item
  Erklären Sie die Kriterien und das Verfahren, das zur Entscheidung verwendet wurde, wie viele Faktoren in der endgültigen Lösung extrahiert wurden und welche Items ausgewählt wurden. Erläutern Sie deutlich die Begründung für die wichtigsten Entscheidungen während des EFA-Prozesses.
\item
  Erklären Sie, welche Rotationsmethoden versucht wurden, die Gründe dafür und die Ergebnisse.
\item
  Die endgültigen Faktorladungen sollten in den Ergebnissen in einer Tabelle angegeben werden. In dieser Tabelle sollte auch die Uniqueness (oder Kommunalität) für jede Variable (in der letzten Spalte) angegeben werden. Die Faktorladungen sollten zusätzlich zu den Nummern der Items mit beschreibenden Kennzeichnungen angegeben werden. Korrelationen zwischen den Faktoren sollten ebenfalls angegeben werden, entweder am Ende dieser Tabelle oder in einer separaten Tabelle.
\item
  Es sollten aussagekräftige Namen für die extrahierten Faktoren angegeben werden. Möglicherweise möchten Sie zuvor ausgewählte Faktorbezeichnungen verwenden, aber nach Prüfung der tatsächlichen Artikel und Faktoren halten Sie vielleicht eine andere Bezeichnung für geeigneter.
\end{enumerate}

\hypertarget{hauptkomponentenanalyse}{%
\chapter{Hauptkomponentenanalyse}\label{hauptkomponentenanalyse}}

PCA

\hypertarget{konfirmatorische-faktorenanalyse}{%
\chapter{Konfirmatorische Faktorenanalyse}\label{konfirmatorische-faktorenanalyse}}

CFA

\hypertarget{multi-trait-multi-method}{%
\chapter{Multi-Trait-Multi-Method}\label{multi-trait-multi-method}}

MTMM

\hypertarget{reliabialituxe4tsanalyse}{%
\chapter{Reliabialitätsanalyse}\label{reliabialituxe4tsanalyse}}

RA

\hypertarget{quellen}{%
\chapter{Quellen}\label{quellen}}

Weiterführende Quellen

  \bibliography{book.bib}

\end{document}
